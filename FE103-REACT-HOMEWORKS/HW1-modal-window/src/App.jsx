import React, { useState } from "react";
import Modal from "./components/Modal";
import Button from "./components/Button";
import Wrapper from "./components/Wrapper";
import "./App.css";
import { modalComponent } from "./halpers/modalContent";
// import modalCont from "./halpers/modalContent.js"

function App() {
  const [modalActive, setModalActiv] = useState(null);
  // const [modalActive2, setModalActiv2] = useState(false);
  const hendleModal = (event) => {
    // event.target.dataset.id;
    console.log(event.target);
    console.log(modalComponent.forEach(element =>element.id==event.target.dataset.id && setModalActiv(element) ));


    // setModalActiv(!modalActive);
  };
  // const hendleModal2 = (event) => {
  //   // setModalActiv2(!modalActive2);
  //   console.log(event.target.dataset.id);
  // };




  return (
    <div className="app">
      <main>
        <Wrapper className= "btn-container">
          <Button
            id="firstBtn"
            name="Open first modal"
            backgroundColor="red"
            onClick={hendleModal}
          />
          <Button
            id="secondBtn"
            backgroundColor="green"
            name="Open second modal"
            onClick={hendleModal}
          />
        </Wrapper>
      </main>
      {modalActive&&<Modal
        active={modalActive}
        setActive={setModalActiv}

      >
        <Button name="Button 1" />
        <Button name="Button 2" />
      </Modal>}
      {/* <Modal
        className="modalBtnSecond"
        active={modalActive2}
        setActive={setModalActiv2}
        title="You pressed the second button"
        text="AAAAAAAAAAAAAAAA"
      >
        <Button name="Button 1" />
        <Button name="Button 2" />
      </Modal> */}
    </div>
  );
}

export default App;
