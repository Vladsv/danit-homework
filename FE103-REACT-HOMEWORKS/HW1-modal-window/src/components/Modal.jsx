import React from "react";
import "./modal.scss";
import Button from "./Button";

const Modal = (props) => {
  const {
    active,
    setActive,
    backgroundColor,
    children,
  } = props;

  const {className, title, text} = active;
  return (
    <div
      className={active ? "modal active" : "modal"}
      onClick={() => setActive(false)}
    >
      <div
        className={`${
          active ? "modal__content active" : "modal__content"
        } ${className}`}
        style={{ backgroundColor }}
        onClick={(e) => e.stopPropagation()}
      >
        <div className="modal_header">
          <Button name ="X"/>
          {/* <button onClick={() => setActive(false)}>X</button> */}
          <h1>{title}</h1>
        </div>

        <p>{text}</p>
        <div className="modal_wrapperBtn">
          {children}
          </div>
      </div>
    </div>
  );
};

export default Modal;
