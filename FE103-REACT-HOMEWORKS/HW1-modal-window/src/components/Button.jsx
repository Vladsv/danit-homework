import React from 'react';
import './button.scss';

function Button (props) {
    const {backgroundColor,name, id} =props;
    return(
        <button data-id = {id} className='btn' style={{backgroundColor}} onClick={props.onClick}>{name}</button>
    )
}
export default Button;
