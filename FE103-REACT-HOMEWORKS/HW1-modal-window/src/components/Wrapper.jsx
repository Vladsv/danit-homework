import React from "react";
import "./wrapper.scss";
function Wrapper(props) {
  const { children, className } = props;
  return <div className={className}>{children}</div>;
}

export default Wrapper;
