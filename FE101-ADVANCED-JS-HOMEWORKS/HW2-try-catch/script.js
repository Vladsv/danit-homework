//Теоретичне питання.
//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// Конструкцию try...catch следует применять если ожидаемые данные могут быть не корректыми,
// принимаются от пользователя, сервера и т.д.

//Завдання
/*Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список 
(схоже завдання виконувалось в модулі basic).
Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність 
(в об'єкті повинні міститися всі три властивості - author, name, price).
Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.*/

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.querySelector("#root");
const list = document.createElement("ul");

function errorBooks(objBook) {
  let {author, name, price} = objBook;
  try {
    if(!author) {
      throw new Error("the author field is missing");
    }
    if (!name){
      throw new Error("the name field is missing");
    }
    if (!price){
      throw new Error("the price field is missing");
    }
  } catch (error) {
    console.log(error);
  }
}

function render (arr) {
arr.forEach((book) => {

  errorBooks(book);

    for (let [key, value] of Object.entries(book)) {
  const item = document.createElement("li");
  item.innerText = `${key} : ${value} \n`;
  list.append(item)
}
const br = document.createElement("br");
list.append(br)
})
}
root.append(list);
render(books);

