// ## Теоретический вопрос
// Обьясните своими словами, как вы понимаете асинхронность в Javascript
// асинхронность это возможность управления  исполнением фрагментов кода не последовательно (в потоке), а в нужном
// мне порядке. 

// ## Задание
// Написать программу "Я тебя по айпи вычислю"

// #### Технические требования:
// - Создать простую HTML страницу с кнопкой `Вычислить по IP`.
// - По нажатию на кнопку - отправить AJAX запрос по адресу `https://api.ipify.org/?format=json`, получить оттуда IP адрес клиента.
// - Узнав IP адрес, отправить запрос на сервис `https://ip-api.com/` и получить информацию о физическом адресе.
// - Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.
// - Все запросы на сервер необходимо выполнить с помощью async await.

// ## Примечание
// Задание должно быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.
const urlUserIp = 'https://api.ipify.org/?format=json';
const urlGetInfo = `http://ip-api.com/json/`;
const button = document.querySelector('button');
const arrPropUser = ["continent", "country", "region", "city", "regionName"];

function getRequest (url) {
    return fetch(url).then(res => res.json().catch(err => console.log(err)))

}
function renderListObj(obj, arrProp, root) {
    const ul = document.createElement('ul');
    arrProp.forEach(elemProp => {
    for (let key in obj){
        if(key == elemProp){
        const li = document.createElement('li');
        li.innerText = `${key} : ${obj[key]}`;
        ul.append(li);}
    }});
    return root.after(ul);
}

async function userInfo(url1, url2) {
   const data = await getRequest(url1);
   const info = getRequest (`${url2}${data.ip}?fields=status,continent,country,region,city,regionName`);
   return info
}

button.addEventListener('click',() => {
    userInfo(urlUserIp, urlGetInfo).then(res => renderListObj(res, arrPropUser, button));
});
