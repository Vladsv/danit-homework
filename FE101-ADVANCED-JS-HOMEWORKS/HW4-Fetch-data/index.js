// ## Теоретический вопрос
// Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript. 

// Это технология запросов на сервер. Особенность и полезность в том, что при запросе не происходит
// перезагрузки страницы.

// ## Задание
// Получить список фильмов серии `Звездные войны`, и вывести на экран список персонажей для каждого из них.

// #### Технические требования:
// - Отправить AJAX запрос по адресу `https://ajax.test-danit.com/api/swapi/films` и получить список всех фильмов серии `Звездные войны`
// - Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства `characters`.
// - Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля `episodeId`, `name`, `openingCrawl`).
// - Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.

const requestUrl = 'https://ajax.test-danit.com/api/swapi/films';
const root = document.querySelector(".root");

function sendRequestFilms(url) {
    return fetch(url, {method : 'GET'})
    .then(response => response.json() )
}

sendRequestFilms(requestUrl)
.then(data => { data.forEach((el)=> {
    console.log(el);
    
    const filmInfo = document.createElement('h4');
    const filmDescription = document.createElement('p')
    const titleActors = document.createElement('h5');
    titleActors.innerText = "Cast of characters";
    const ul = document.createElement('ul');
    filmInfo.textContent =`Episode - ${el.episodeId} Name - ${el.name}`;
    filmDescription.textContent = `Description - ${el.openingCrawl}`;
    el.characters.forEach(actor => {
        sendRequestFilms(actor)
        .then(data => {
            console.log(data.name);
            const li = document.createElement ('li');
           li.textContent = `${data.name}`;
           ul.append(li)
        })
    })
    
    root.append(filmInfo,filmDescription,titleActors,ul)
    
});
    
})
.catch(err => console.log(err));
// console.log(filmsAll);
