// Теоретичне питання
// 1) Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Это способность одного обьекта расширять свои свойства и методы из другого обьекта (наследовать их)

// 2) Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Для того чтобы вызвать конструктор у класса-родителя, тем самым использовать его свойства и методы.

//Завдання
// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  set name(value) {
    this._name = value;
  }
  get name() {
    return this._name;
  }

  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }

  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary * 3;
  }
}

const juniorProgrammer = new Programmer("Nik", 20, 1000, [
  "English",
  "Ukrainian",
  "JavaScript",
]);
const middleProgrammer = new Programmer("Olga", 30, 3000, [
  "English",
  "Ukrainian",
  "JavaScript",
]);
const seniorProgrammer = new Programmer("Oleg", 40, 5000, [
  "English",
  "Ukrainian",
  "JavaScript",
]);
console.log(juniorProgrammer, middleProgrammer, seniorProgrammer);
console.log(juniorProgrammer.salary);
