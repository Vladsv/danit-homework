let tabs = document.querySelector(".tabs");
let tabsTitle = document.querySelectorAll(".tabs-title");
let paragraphText = document.querySelectorAll(".tabs-content li");
tabs.addEventListener("click", (event) => {
  tabsTitle.forEach((el, index) => {
    if (el == event.target) {
      paragraphText[index].style.display = "block";
    } else {
      paragraphText[index].style.display = "none";
    }
  });
});
