// Теория
// 1) Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования

// экранирование нужно чтобы не нарушать синтаксис програмного кода. Нужно для того, чтобы
// компилятор понимал, где символы являются чвстью кода, а где - чвстью текста.

//2)Які засоби оголошення функцій ви знаєте?
// Три способа - function declaration, function expression и стрелочная функция

// 3)Що таке hoisting, як він працює для змінних та функцій?
// Всплытие, использование переменных и функций до их обьявления. Лечится переходом в строгий режим.


function сreateNewUser() {
  let nameUser = prompt("Введите Ваше имя", "Иван");
  let lastNameUser = prompt("Введите Вашу фамилию", "Иванов");
  let birthdayUser = prompt("Введите дату рождения в формате 'дд.мм.гггг'");
  const newUser = {
    firstName: nameUser,
    lastName: lastNameUser,
    birthday: birthdayUser,
    getAge() {
      let newDate = new Date();
      const currentYear = newDate.getFullYear();
      const currentMonth = newDate.getMonth()+1;
      const currentDay = newDate.getDate();
      const birthYearNumber =  this.birthday.slice(-4);
      if(+this.birthday.slice(3,5) < currentMonth){
        return currentYear - birthYearNumber;
      }else if (+this.birthday.slice(0,2)<=currentDay){
      return currentYear - birthYearNumber;
      }else {return currentYear - birthYearNumber - 1};
    },
    getPassword() {
      return `${this.firstName[0].toLocaleUpperCase()}${this.lastName.toLowerCase()}${this.birthday.slice(-4)}`;
    },
    getLogin() {
      return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
    },
  };
  Object.defineProperty(newUser, "firstName", {
    writable: false,
    configurable: true,
  });
  Object.defineProperty(newUser, "lastName", {
    writable: false,
    configurable: true,
  });
  newUser.setNewFirstName = function (nameUser) {
    Object.defineProperty(this, "firstName", { value: nameUser });
  };
  newUser.setNewLastName = function (lastNameUser) {
    Object.defineProperty(this, "lastName", { value: lastNameUser });
  };
  return newUser
};
let myUser = сreateNewUser(); // Создали объект
// myUser.firstName = "Vlad"; // Попытались напрямую перезаписать имя на другое.
// myUser.lastName = "Slobodianuk"; // Попытались напрямую перезаписать фамилию на другую.
// console.log(myUser); // Не смотря на попытки перезаписать имя / фамилию - они остались старыми, так как writable: false
// myUser.setNewFirstName("Vlad"); // Пытаемся перезаписать имя через метод
// myUser.setNewLastName("Slobodianuk"); // Пытаемся перезаписать фамилию через метод
// console.log(myUser); // Имя и фамилия теперь новые, потому что configurable: true и через повторные вызовы Object.defineProperty они перезаписываются, а напрямую - нет.
console.log(myUser);
console.log(myUser.getAge());
console.log(myUser.getPassword());
console.log(myUser.getLogin());
