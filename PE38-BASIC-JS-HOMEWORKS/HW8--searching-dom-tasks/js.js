
// Теория 
//  1) Опишіть своїми словами що таке Document Object Model (DOM)?
//  Это дерево документа (страницы), представленное в виде Нод (по сути обьектов, которые можно менять)

//  2) Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// HTML-елемент это тег обьявленный в HTML разметке или созданный и вставленный посредством JS.
// innerHTML - это свойство интерфейса Element для установки или получения HTML разметки.
// innerText - это свойство интерфейса Node для установки или получения содержимого HTML-елементов.

//  3) Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//  по ноде, тегу, id и классу. Лучшего нет! как говорит наш преподаватель Роман - "просто потому что можем!" 




// 1) Найти все параграфы на странице и установить цвет фона #ff0000

let paragraph = document.querySelectorAll("p");
paragraph.forEach((value)=> {value.style.backgroundColor = "#ff0000"})
console.log(paragraph);

// 2) Найти элемент с id="optionsList". Вывести в консоль. Найти родительский
// элемент и вывести в консоль. Найти дочерние ноды, если они есть, и вывести в консоль названия и тип нод.

const optionsList = document.querySelector("#optionsList");
console.log(optionsList);
console.log(optionsList.parentElement);
optionsList.childNodes.forEach((element) => {
    if (!element) {
        console.log("Has no Node");
    }
    else{
        console.log(`Name: ${element}, type: ${element.nodeType}`);
    }
});

// 3) Установите в качестве контента элемента с классом testParagraph следующий параграф <p>This is a paragraph</p>
// в коде нет элемента с классом testParagraph есть только id !

const testParagraph = document.querySelector("#testParagraph");
testParagraph.innerHTML = '<p>This is a paragraph</p>'

testParagraph.textContent = 'This is a paragraph';

// либо так:
testParagraph.parentElement.insertAdjacentHTML('beforeend', 
'<p class="testParagraph">This is a paragraph</p>');

// либо так:
testParagraph.classList.add("testParagraph");
// не понятно по условию что нужно сделать((


// 4) Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const mainHeader = document.querySelector('.main-header');
for (let child of mainHeader.children){
  child.classList.add("nav-item");
  console.log(child);

};


// 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const sectionTitle = document.querySelectorAll(".section-title");
for(let elem of sectionTitle) {
  elem.classList.remove("section-title")
};
