// Теоретичні питання
// 1)Опишіть своїми словами як працює метод forEach.
//  forEach перебирает массив и чрез колбэк функцию у нас появляется доступ к каждому элементу - его значению и индексу.
//  forEach ничего не возвращает.
// 2)Як очистити масив?
//  arr.length = 0; это самый простой способ.
// 3)Як можна перевірити, що та чи інша змінна є масивом?
// Array.isArray(arr) проверка на массив.

let arrDataType = ["undefined", "number", "boolean", "string", "object", "function"]
let myArr = ['hello', 500, undefined, 0, '5555', true, function(){}, {}];
filterBy = (arr, type) => {
  let newArr = arr.filter(element => typeof element != type )
return newArr
};
funcTest =(arrFilter, arrDataType)=>{
  for (let i = 0; i < arrDataType.length; i++) {
    console.log(filterBy(arrFilter,arrDataType[i]), `no type: ${arrDataType[i]}`);
  }
}
funcTest(myArr, arrDataType)

//второй способ - forEach...............................................................
console.log('Второй вариант');

filterByForEach = (arr, type) => {
const newArrFilter = [];
arr.forEach((value)=> {
  if(typeof value != type)
  newArrFilter.push(value);
});
return newArrFilter
}
funcTest =(arrFilter, arrDataType)=>{
  for (let i = 0; i < arrDataType.length; i++) {
    console.log(filterByForEach(arrFilter,arrDataType[i]), `no type: ${arrDataType[i]}`);
  }
}
funcTest(myArr, arrDataType)
