/*
Задание
Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

В файле index.html лежит разметка для двух полей ввода пароля.
По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения

После нажатия на кнопку страница не должна перезагружаться
Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.
*/

const form = document.forms.passwords;
const inputLast = document.querySelector(".input-last");
const btn = document.querySelector(".btn");
const iconPassword = document.querySelectorAll(".icon-password");

// function showPassword(element) {
//   if (element.classList.contains("fa-eye")) {
//     element.closest(".input-wrapper").children[0].setAttribute("type", "text");
//     element.style.display = "none";
//     element.closest(".input-wrapper").children[2].style.display = "block";
//   } else if (element.classList.contains("fa-eye-slash")) {
//     element.closest(".input-wrapper").children[0].setAttribute("type", "password");
//     element.style.display = "none";
//     element.closest(".input-wrapper").children[1].style.display = "block";
//   }
// }

function showPassword(element) {
  console.log(element);
  element.classList.toggle("fa-eye-slash");
  element.classList.toggle("fa-eye");
}


function comparePassword (){
  if(form.enter.value !=""){
   form.enter.value === form.confirm.value;
  } else {return false;}
}
function showMessage(valid) {
  const paragraph = document.createElement("p");
  paragraph.classList.add("massage-error");
  const massageError = document.querySelector(".massage-error");
  paragraph.textContent = "Нужно ввести одинаковые значения";
  paragraph.style.cssText = `color: red; margin-top: -25px`;
  if(valid) {
    massageError.remove();
    alert("You are welcome");
    return
  }if(massageError||massageError!=null){
    return
  }
  else{
    inputLast.after(paragraph);
  };
  
}  

// iconPassword.addEventListener("click", (event) => {
//   console.log(event);
//   // showPassword(event.target);
// });
iconPassword.addEventListener("click", (event) => {
  let elem = event.target;
  if(elem.classList.contains("icon-password")){
    elem.classList.toggle("fa-eye-slash");
    elem.classList.toggle("fa-eye");
  }
  
  // console.log(elem);

  // if(elem.classList.contains("fa-eye")){
  //   console.log("ok1");
  //   let inputPass = elem.closest(".input")
  //   console.log(inputPass);
  //   inputPass.setAttribute("type", "text");
  // }else{ 
  //   console.log("ok2");
  //   let inputPass = elem.closest(".input")
  //   console.log(inputPass);
  //   elem.closest(".input").setAttribute("type", "password");
  // }

  if(elem.classList.contains("fa-eye")){
      
      elem.closest(".input").setAttribute("type", "text");
      console.log(elem.closest(".input"));
  }
  console.log(elem.closest(".input"));
});


form.addEventListener("submit", (event)=>{
event.preventDefault()
showMessage(comparePassword());

})
